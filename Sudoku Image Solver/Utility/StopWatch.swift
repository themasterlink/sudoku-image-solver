//
//  StopWatch.swift
//  ImageUnderstanding
//
//  Created by Max on 26.06.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

class StopWatch {
    
    var m_start : CFTimeInterval = 0;
    var m_end : CFTimeInterval = 0;
    
    var m_name : String = "";
    
    convenience init(name : String){
        self.init();
        m_name = name;
    }
    
    init(){
        m_start = CFAbsoluteTimeGetCurrent();
        m_end = CFAbsoluteTimeGetCurrent();
    }
    
    func elapsedTime() -> CFTimeInterval {
        return CFAbsoluteTimeGetCurrent() - m_start;
    }
    
    func printElapsedTime(){
        print("\(m_name) with \(elapsedTime())");
    }
    
    func stopTime() -> CFTimeInterval {
        m_end = CFAbsoluteTimeGetCurrent();
        return m_end - m_start;
    }
    
    func resetTime(){
        m_start = CFAbsoluteTimeGetCurrent();
        m_end = CFAbsoluteTimeGetCurrent();
    }
    
    func printEleAndReset(name : String){
        printElapsedTime();
        resetTime();
        m_name = name;
    }
}