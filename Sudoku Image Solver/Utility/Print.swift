//
//  Print.swift
//  Sudoku Image Solver
//
//  Created by Max on 02.10.15.
//  Copyright © 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

func printInfo( line : String, function : String = #function, file : String =  #file, codeLine : Int = #line) {
    var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
    pathPrefix.removeRange(Range<String.Index>(pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6) ..< pathPrefix.endIndex));
    let output1 = "In " + pathPrefix + "::";
    let output2 = function + "()::\(codeLine): " + line;
    print(output1 + output2);
}

func printDebug( line : String, function : String = #function, file : String =  #file, codeLine : Int = #line) {
    if(isDebug){
        var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
        pathPrefix.removeRange(Range<String.Index>(pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6) ..< pathPrefix.endIndex));
        let output1 = "In " + pathPrefix + "::";
        let output2 = function + "()::\(codeLine): " + line;
        print(output1 + output2);
    }
}

func printError( line : String, function : String = #function, file : String =  #file, codeLine : Int = #line) {
    var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
    pathPrefix.removeRange(Range<String.Index>(pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6) ..< pathPrefix.endIndex));
    let output1 = "Error in " + pathPrefix + "::";
    let output2 = function + "()::\(codeLine): " + line;
    print(output1 + output2);
}

func printErrorIf( line : String, isError : Bool, function : String = #function, file : String =  #file, codeLine : Int = #line) {
    if(isError){
        var pathPrefix = NSURL(fileURLWithPath: file).lastPathComponent!;
        pathPrefix.removeRange(Range<String.Index>(pathPrefix.startIndex.advancedBy(pathPrefix.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) - 6) ..< pathPrefix.endIndex));
        let output1 = "Error in " + pathPrefix + "::";
        let output2 = function + "()::\(codeLine): " + line;
        print(output1 + output2);
    }
}
