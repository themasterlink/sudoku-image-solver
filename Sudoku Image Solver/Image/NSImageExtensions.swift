//
//  UIImageExtensions.swift
//  ImageUnderstanding
//
//  Created by Max on 26.06.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation
import SpriteKit

extension NSImage {
//    var CGImage : CGImageRef {
//        get {
//            let imageData = self.TIFFRepresentation;
//            let source = CGImageSourceCreateWithData(imageData as! CFDataRef, nil)//.takeUnretainedValue();
//            let maskRef = CGImageSourceCreateImageAtIndex(source, 0, nil);
//            return maskRef//.takeUnretainedValue();
//        }
//    }
    
    func pixelData() -> [[PixelData]] {
        if(self.representations.count == 0){
            return [[PixelData]]();
        }
        let bmp = self.representations[0] as! NSBitmapImageRep
        var data: UnsafeMutablePointer<UInt8> = bmp.bitmapData
        var r, g, b, a: UInt8
        var pixels = [[PixelData]](count: bmp.pixelsHigh, repeatedValue: [PixelData](count: bmp.pixelsWide, repeatedValue: PixelData()));
//        let the_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        let xS = bmp.pixelsHigh;
//        let yS = bmp.pixelsWide;
//        pixels.withUnsafeMutableBufferPointer( { valuePtr -> Void in
//            dispatch_apply(xS, the_queue) { outerIdx -> Void in
//                let y = outerIdx;
//                let ySize = yS;
//                valuePtr[Int(outerIdx)].withUnsafeMutableBufferPointer( { innerValuePtr -> Void in
//                    dispatch_apply(ySize, the_queue) { innerOuterIdx -> Void in
//                        let x = innerOuterIdx;
//                        var pixelInfo: Int = ((ySize * y) + x) * 4;
//                        // println("Line: \(x), \(y)");
//                        var r = (data[pixelInfo])
//                        var g = (data[pixelInfo+1])
//                        var b = (data[pixelInfo+2])
//                        innerValuePtr[x] = PixelData(a: 255, r: r, g: g, b: b);
//                        //(r+g+b) / 3;
//                        //let col : Color = img.getPixelColor(x: innerOuterIdx, y: outerIdx);
//                        //innerValuePtr[Int(innerOuterIdx)] = col.grey;
//                    }
//                } );
//            }
//        } )
        for row in 0..<bmp.pixelsHigh {
            for col in 0..<bmp.pixelsWide {
                r = data.memory
                data += 1;
                //                data = data.advancedBy(1)
                g = data.memory
                data += 1;
                //                data = data.advancedBy(1)
                b = data.memory
                data += 1;
                //                data = data.advancedBy(1)
                a = data.memory
                data += 1;
                //                data = data.advancedBy(1)
                pixels[row][col].a = a;
                pixels[row][col].r = r;
                pixels[row][col].g = g;
                pixels[row][col].b = b;
            }
        }

        return pixels
    }
    
    func greyPixelData() -> [[GreyPixelData]] {
        if(self.representations.count == 0){
            return [[GreyPixelData]]();
        }
        let bmp = self.representations[0] as! NSBitmapImageRep
        var data: UnsafeMutablePointer<UInt8> = bmp.bitmapData
        var r, g, b, a: UInt8
        var pixels = [[GreyPixelData]](count: bmp.pixelsHigh, repeatedValue: [GreyPixelData](count: bmp.pixelsWide, repeatedValue: GreyPixelData()));
        for row in 0..<bmp.pixelsHigh {
            for col in 0..<bmp.pixelsWide {
                r = data.memory
                data += 1;
                g = data.memory
                data += 1;
                b = data.memory
                data += 1;
                a = data.memory
                data += 1;
                pixels[row][col].a = a;
                pixels[row][col].setWith(r, g, b);
            }
        }
        return pixels
    }

}


//extension NSImage {
//    func getPixelColor(#x: Int, y: Int) -> Color {
//
//        var pixelData = CGDataProviderCopyData(CGImageGetDataProvider(self.CGImage))
//        var data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
//        
//        var pixelInfo: Int = ((Int(self.size.width) * y) + x) * 4
//        
//        var r = Int(data[pixelInfo])
//        var g = Int(data[pixelInfo+1])
//        var b = Int(data[pixelInfo+2])
//        var a = Int(data[pixelInfo+3])
//        
//        return Color(red : r, green : g, blue : b , alpha : a);
//        //return UIColor(red: r, green: g, blue: b, alpha: a)
//    }
//}

public struct PixelData {
    
    var a:UInt8 = 255
    var r:UInt8 = 0;
    var g:UInt8 = 0;
    var b:UInt8 = 0;
    
    init(){
    }
    
    init(pxl : PixelData){
        self.a = pxl.a;
        self.r = pxl.r;
        self.g = pxl.g;
        self.b = pxl.b;
    }
    
    init(greyPxl: GreyPixelData){
        self.a = greyPxl.a;
        self.r = greyPxl.g;
        self.g = greyPxl.g;
        self.b = greyPxl.g;
    }
    
    init(a : UInt8, r : UInt8, g: UInt8, b: UInt8){
        self.a = a;
        self.r = r;
        self.g = g;
        self.b = b;
    }
    
    init(grey: UInt8){
        r = grey;
        g = grey;
        b = grey;
    }
    
    var grey : UInt8 {
        return UInt8( (Int(r) + Int(g) + Int(b)) / 3 );
    }
}

public struct GreyPixelData {
    
    var a:UInt8 = 255;
    var g:UInt8 = 0;
    
    init(){}
    
    init(a : UInt8, grey : UInt8){
        self.a = a;
        self.g = grey;
    }
    
    init(grey: UInt8){
        g = grey;
    }
    
    mutating func setWith(r : UInt8, _ g : UInt8, _ b : UInt8){
        self.g = UInt8((Int(r) + Int(g) + Int(b)) / 3);
    }
}

private let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
private let bitmapInfo:CGBitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedFirst.rawValue)


public func imageFromARGB32Bitmap(pixels: [PixelData], width: Int, height: Int) -> NSImage {
    let bitsPerComponent:UInt = 8
    let bitsPerPixel:UInt = 32;
    assert(pixels.count == Int(width * height))
    
    var data = pixels // Copy to mutable []
    let providerRef = CGDataProviderCreateWithCFData(
        NSData(bytes: &data, length: data.count * sizeof(PixelData))
    )
    let t : UnsafePointer<CGFloat> = nil;
    let cgim = CGImageCreate(width, height, Int(bitsPerComponent), Int(bitsPerPixel), Int(width * Int(sizeof(PixelData))), rgbColorSpace, bitmapInfo, providerRef, t, true, CGColorRenderingIntent.RenderingIntentDefault);
    return NSImage(CGImage: cgim!, size: CGSize(width: width, height: height));
}

public func imageFromAGrey32Bitmap(pixels: [GreyPixelData], width: Int, height: Int) -> NSImage {
    var data = [PixelData](count: pixels.count, repeatedValue: PixelData());
    var i = 0;
    for ele in pixels {
        data[i] = PixelData(greyPxl: ele);
        i += 1;
    }
    return imageFromARGB32Bitmap(data, width: width, height: height);
}