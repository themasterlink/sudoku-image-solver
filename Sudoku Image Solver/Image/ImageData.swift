//
//  ImageData.swift
//  ImageUnderstanding
//
//  Created by Max on 26.06.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation
import SpriteKit

class ImageData {

    var m_val : [[PixelData]] = [];
    let m_xS : Int;
    let m_yS : Int;
    
    init(cpy : ImageData){
        m_xS = cpy.m_xS
        m_yS = cpy.m_yS
        m_val = [[PixelData]](cpy.m_val);
    }
    
    init(xSize : Int, ySize : Int){
        m_xS = xSize;
        m_yS = ySize;
        let pxl = PixelData(a: 255, r: 255, g: 0, b: 0);
        m_val = [[PixelData]](count: m_xS, repeatedValue: [PixelData](count: m_yS, repeatedValue: pxl));
    }
    
    init(xSize : Int, ySize : Int, img : NSImage){
        m_xS = xSize;
        m_yS = ySize;
        setWith(img);
    }
    
    func setWith(img : NSImage) {
        m_val = img.pixelData();
    }
    
    func getNSImage() -> NSImage {
        let p = PixelData(a : 255, r : 0, g: 0, b: 0);
//        print("\(m_xS), \(m_yS)");
        var pixels : [PixelData] = [PixelData](count: m_xS * m_yS, repeatedValue: p);
        var i = 0;
//        print("\(pixels.count)");
        for x in 0 ..< m_xS {
            for y in 0 ..< m_yS {
                pixels[i] = m_val[x][y];
                i += 1;
            }
        }
        return imageFromARGB32Bitmap(pixels, width: m_yS, height: m_xS);
    }
}

class GreyImageData {
    
    var m_val : [[GreyPixelData]] = [];
    let m_xS : Int;
    let m_yS : Int;
    
    
    init(xSize : Int, ySize : Int){
        m_xS = xSize;
        m_yS = ySize;
        let pxl : GreyPixelData = GreyPixelData();
        m_val = [[GreyPixelData]](count: m_xS, repeatedValue: [GreyPixelData](count: m_yS, repeatedValue: pxl));
    }
    
    init(xSize : Int, ySize : Int, img : NSImage){
        m_xS = xSize;
        m_yS = ySize;
        setWith(img);
    }
    
    func setWith(img : NSImage) {
        m_val = img.greyPixelData();
    }
    
    func getNSImage() -> NSImage {
        var pixels : [PixelData] = [PixelData](count: m_xS * m_yS, repeatedValue: PixelData());
        var i = 0;
        for x in 0 ..< m_xS {
            for y in 0 ..< m_yS {
                pixels[i] = PixelData(grey: m_val[x][y].g);
                i += 1;
            }
        }
        return imageFromARGB32Bitmap(pixels, width: m_yS, height: m_xS);
    }
}