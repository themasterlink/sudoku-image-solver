//
//  Image.swift
//  ImageUnderstanding
//
//  Created by Max on 26.06.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation
import SpriteKit


func avgInt(numbers:Int...) -> Int {
    return Int(numbers.reduce(0,combine: +))/Int(numbers.count)
}

class Image {

    /*   ------> Breite Y // Spalten, Columns
        |
        |
        |
        \/
        Höhe X, Zeilen, Rows
    */
    var m_img : ImageData;
    let m_xSize : Int;
    let m_ySize : Int;
    
    var m_reg : [BowString] = [];
    
    init(xSize : Int, ySize : Int){
        m_xSize = xSize;
        m_ySize = ySize;
        m_img = ImageData(xSize: m_xSize, ySize: m_ySize);
//        for var x = 0; x < m_xSize; x++ {
//            for var y = 0; y < m_ySize; y++ {
//                m_img.m_val[x][y] = PixelData(grey: UInt8(((y * m_xSize) + x) / 255));
//            }
//        }
    }
    
    init(file : String){
        let img = NSImage(contentsOfFile: file);
        var image : NSImage;
        if(img != nil){
            image = img!;
        }else{
            assert(true, "There is no NSImage!");
            image = NSImage();
            m_xSize = 0;
            m_ySize = 0;
            m_img = ImageData(xSize: m_xSize, ySize: m_ySize);
            return;
        }
//        m_img = ImageData(xSize: m_xSize, ySize: m_ySize);
//        for var x = 0; x < m_xSize; x++ {
//            for var y = 0; y < m_ySize; y++ {
//                m_img.m_val[x][y] = PixelData(grey: UInt8((y) % 255));
//            }
//        }
        let bmp = image.representations[0] as! NSBitmapImageRep
        m_xSize = bmp.pixelsHigh;
        m_ySize = bmp.pixelsWide;
        m_img = ImageData(xSize: m_xSize, ySize: m_ySize, img: image);
    }
    
    func stampAt(x  x: Int, y: Int, pxl : PixelData) {
        setValAt(x, y: y, val: pxl);
        setValAt(x + 1, y: y, val: pxl);
        setValAt(x, y: y + 1, val: pxl);
        setValAt(x + 1, y: y + 1, val: pxl);
        setValAt(x - 1, y: y + 1, val: pxl);
        setValAt(x + 1, y: y - 1, val: pxl);
        setValAt(x - 1, y: y, val: pxl);
        setValAt(x, y: y - 1, val: pxl);
        setValAt(x - 1, y: y - 1, val: pxl);
    }
    
    func findFastNumbers(inout grid: Grid){
        if(m_xSize == 0 && m_ySize == 0){
            printError("The image has to be inited before it is used!");
            return;
        }
        let sw = StopWatch(name: "multi: ");
//        normTo(seperateValue: 128);
        
        for line in m_img.m_val {
            for var ele in line {
                if(ele.grey >= 128){
                    ele = PixelData(grey: 255);
                }else{
                    ele = PixelData(grey: 0);
                }
            }
        }
        
        sw.printElapsedTime();
        
    }
    
    
    func findNumbers(inout grid : Grid){
        if(m_xSize == 0 && m_ySize == 0){
            return;
        }
        self.normTo(seperateValue: 128);
        globalAppRef?.showImage(self);
        
        
        let level = 5;
        let lastLevel = 2;
        var imgPyr = self.getPyramideImage(level: level);
//        for i in 0 ... level {
//            globalAppRef?.showImage(imgPyr[i]);
//        }
//        var template = Image(file: "/Users/Max/Documents/XCode_Projects/Bitbucket/Sudoku Image Solver/pics/1.jpg");
//        template.normTo(seperateValue: 128);
//        var temPyr = template.getPyramideImage(level: level);
        func lower(lhs : UInt8, rhs : UInt8) -> Bool {
            return lhs < rhs;
        }
        func higher(lhs : UInt8, rhs : UInt8) -> Bool {
            return lhs > rhs;
        }
        // on the whole image
//        let histo = UInt8(imgPyr[level - 1].getGreyValueInHistoForBlack(0.5));
        
       
//        print("ActReg: \(actReg)")
//        for bow in actReg {
//            for x in 0 ..< bow.m_len{
//                imgPyr[level - 1].m_img.m_val[bow.m_yb][bow.m_xb+x] = PixelData(a: 255, r: 255, g: 0, b: 0);
//            }
//        }
//        globalAppRef?.showImage(imgPyr[level - 1]);
        
        struct Field : CustomStringConvertible {
            
            var description : String {
                return "\(actNumber)";
            }
            var actNumber = 0;
            var certainty : UInt8 = 0;
        }
        
        var m_field = [[Field]](count: 9, repeatedValue: [Field](count: 9, repeatedValue: Field()));
        let startLevel = level - 1;
        var actRegBefore = imgPyr[startLevel].findRegionsGreyValueWithData(252, functor: lower);
//        imgPyr[startLevel].setFor(actRegBefore, to: PixelData(a: 255, r: 0, g: 255, b: 0))
//        globalAppRef?.showImage(imgPyr[startLevel]);
//        return;
        let regions = seperateRegions(forBows : actRegBefore, withClosing: 10);
        actRegBefore = [];
        for reg in regions {
            var minX = Int.max;
            var minY = Int.max;
            var maxX = 0;
            var maxY = 0;
            for bow in reg {
                if(bow.m_xb < minX){
                    minX = bow.m_xb;
                }
                if(bow.m_xb > maxX){
                    maxX = bow.m_xb;
                }
                if(bow.m_yb + bow.m_len > maxY){
                    maxY = bow.m_yb + bow.m_len;
                }
                if(bow.m_yb < minY){
                    minY = bow.m_yb;
                }
            }
//            print("max x: \(maxX), max y: \(maxY), min x: \(minX), min y: \(minY)");
            for x in minX ... maxX {
                let newBow = BowStringWithData(xb: x, yb: minY, len: maxY - minY);
                for y in 0 ..< newBow.m_len {
                    newBow.m_data[y] = imgPyr[startLevel].m_img.m_val[newBow.m_xb][newBow.m_yb + y];
                }
                actRegBefore.append(newBow);
            }
        }
        
//        imgPyr[startLevel].setFor(actRegBefore, to: PixelData(a: 255, r: 255, g: 0, b: 0))
//        globalAppRef?.showImage(imgPyr[startLevel]);
//        return;
//        let actRegOnWholeImg : [BowString] = imgPyr[level - 1].findRegionsGreyValueWithData(10, functor: lower);
        var timeForGreyVal = 0.0;
        var counterGreyVal = 0;
        for iNumber in 1 ... 9 {
            let sw3 = StopWatch(name: "reading");
            let template = Image(file: "/Users/Max/Documents/XCode_Projects/Bitbucket/Sudoku Image Solver/pics/\(iNumber).jpg");
            template.normTo(seperateValue: 128);
            var temPyr = template.getPyramideImage(level: level);
            sw3.printElapsedTime();
//            var actReg = [BowStringWithData](count: actRegOnWholeImg.count, repeatedValue: BowStringWithData(cb: 0, rb: 0, len: 0));
//            for var i = 0; i < actRegOnWholeImg.count; i++ {
//                actReg[i].setWith(actRegOnWholeImg[i] as! BowStringWithData);
//            }
            var actReg = [BowStringWithData](count: actRegBefore.count, repeatedValue: BowStringWithData(xb: 0, yb: 0, len: 0));
            for (i,bow) in actRegBefore.enumerate() {
                actReg[i] = BowStringWithData(cpy : bow);
            }
//            if(iNumber == 1){
////                imgPyr[startLevel].setFor(actReg, to: PixelData(a: 255, r: 0, g: 0, b: 255));
//                globalAppRef?.showImage(imgPyr[startLevel]);
//            }
            let sw2 = StopWatch(name: "matchin bow strings");
            for actLevel in (lastLevel ... startLevel).reverse() { // var actLevel = startLevel; actLevel >= lastLevel; actLevel-- { // TODO check level minus or plus!
//                printInfo("Level: \(actLevel)");
//                print("ActReg count: \(actReg.count)");
                let actImg = imgPyr[actLevel];
                let actTemp = temPyr[actLevel];
                if(actLevel > lastLevel){
                    print("bow string match");
                    actReg = actImg.generateMatchingBowStrings(with: actTemp, bows: actReg, withScalingFactor: true);
                    let sw = StopWatch()
                    let newHisto = getGreyValueInHistoForBlackInPyr(0.5, forBows: actReg);
                    actReg = findInRegionsGreyValueWithData(newHisto, forBows: actReg, functor: higher);
                    timeForGreyVal += sw.elapsedTime();
                    counterGreyVal += 1;
                    globalAppRef?.showImage(actImg);
//                    var newReg = [BowStringWithData](count: actReg.count * 2, repeatedValue: BowStringWithData(xb: 0, yb: 0, len: 0));
//                    for var i = 0; i < actReg.count; i++ {
//                        let bowUp = BowStringWithData(xb: actReg[i].m_xb * 2, yb: actReg[i].m_yb * 2, len: actReg[i].m_len * 2);
//                        let bowDown = BowStringWithData(xb: actReg[i].m_xb * 2 + 1, yb: actReg[i].m_yb * 2, len: actReg[i].m_len * 2);
//                        for y in 0 ..< actReg[i].m_len {
//                            let pxl = actReg[i].getPixelData(y: y);
//                            bowUp.m_data[y*2] = pxl;
//                            bowUp.m_data[y*2+1] = pxl;
//                            bowDown.m_data[y*2] = pxl;
//                            bowDown.m_data[y*2+1] = pxl;
//                        }
//                        newReg[i*2] = bowUp;
//                        newReg[i*2+1] = bowDown;
//                    }
//                    actReg = newReg;
                }else{
                    actReg = actImg.generateMatchingBowStrings(with: actTemp, bows: actReg, withScalingFactor: false);
//                    for bow in actReg {
//                        bow.maxContrast();
//                    }
////                    if(iNumber < 3 && actLevel - 1 > 0){
//                        imgPyr[1].setFor(actReg);
//                        globalAppRef?.showImage(imgPyr[1]);
////                    }
                }
//                if(iNumber < 3 && actLevel - 1 > 0){
//                    imgPyr[actLevel - 1].setFor(actReg);
//                    globalAppRef?.showImage(imgPyr[actLevel - 1]);
//                }
//                for bow in actReg {
//                    (bow).maxContrast();
//                }
            }
            sw2.printElapsedTime();
            let regionsAfter = seperateRegions(forBows : actReg, withClosing: 20);
            let xNumberWidth : Double  = Double(imgPyr[lastLevel].m_xSize) / 27.0;
            let yNumberHeight : Double = Double(imgPyr[lastLevel].m_ySize) / 27.0;
            for reg in regionsAfter {
                let center = highestValueInRegion(forBows: reg);
                if(center.x > imgPyr[lastLevel].m_xSize || center.y > imgPyr[lastLevel].m_ySize){
                    printError("\(center): size: (\(imgPyr[lastLevel].m_xSize), \(imgPyr[lastLevel].m_ySize))");
                    continue;
                }
                let newX = Int((Double(center.x) / xNumberWidth ) / 3.0 - 1e-7);
                let newY = Int((Double(center.y) / yNumberHeight) / 3.0 - 1e-7);
//                printInfo("Center: \(center), xWid: \(xNumberWidth * 27), yHei: \(yNumberHeight * 27)");
//                printInfo("New x: \(newX), new y: \(newY) => \(center.highestValue) for \(iNumber)");
                if(center.highestValue > m_field[newX][newY].certainty){
                    m_field[newX][newY].actNumber = iNumber;
                    m_field[newX][newY].certainty = center.highestValue;
                }
            }
        }
        for x in 0 ... 8 {
            for y in 0 ... 8 {
                if(m_field[x][y].actNumber > 0){
                    grid[x,y] = Element(fieldValue: m_field[x][y].actNumber);
                }else{
                    grid[x,y] = Element();
                }
            }
        }
        print("Time needed for grey val: \(timeForGreyVal / Double(counterGreyVal))");
//        globalAppRef?.showImage(imgPyr[0]);
        grid.printGrid();
//                globalAppRef?.showImage(imgPyr[0]);
        //let newImg = imgPyr[level-1].generateMatchingImage(with: temPyr[level-1]);

        
        //            print("actreg : \(actReg)");
        //
        //            globalAppRef?.showImage(actImg);
        //            for bow in actReg {
        //                if bow is BowStringWithData {
        //                    let actBow : BowStringWithData = bow as! BowStringWithData;
        //                    actBow.maxContrast();
        //                    for x in 0 ..< bow.m_len{
        //                        imgPyr[actLevel].m_img.m_val[bow.m_yb][bow.m_xb+x] = actBow.getPixelData(cb: x);
        //                    }
        //                }
        //            }
        //            globalAppRef?.showImage(imgPyr[actLevel]);
        
//        globalAppRef?.showImage(imgPyr[level-2]);
//        var newReg = imgPyr[level-2].generateMatchingBowStrings(with: temPyr[level-2], bows: reg);
//        
//        for bow in newReg {
//            bow.maxContrast();
//        }
//        globalAppRef?.showImage(newerImg);
//        for bow in newReg {
//            for x in 0 ..< bow.m_len {
//                imgPyr[level-2].stampAt(x: x + bow.m_yb, y: bow.m_xb, pxl: bow.m_data[x]);
//            }
//        }
//        globalAppRef?.showImage(imgPyr[level-2]);
    }
    
    func generateTemplate(template : Image) -> Image {
        let level = 3;
        var imgPyr = self.getPyramideImage(level: level);
        var temPyr = template.getPyramideImage(level: level);
//        for i in imgPyr {
//            globalAppRef?.showImage(i);
//        }
        print("img size: \(imgPyr.count)");
        let img = imgPyr[level-1];
//        let xSize = img.m_xSize;
//        let ySize = img.m_ySize;
//        var result = Image(xSize: xSize, ySize: ySize);
//        var temp = temPyr.first!;
        let newImg = img.generateMatchingImage(with: temPyr[level-1]);
        return newImg;
    }
    
    func normTo(seperateValue  seperateValue : UInt8){
        let the_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        let xS = m_xSize;
        let yS = m_ySize;
        m_img.m_val.withUnsafeMutableBufferPointer( { valuePtr -> Void in
            dispatch_apply(xS, the_queue) { outerIdx -> Void in
//                let y = outerIdx;
                let ySize = yS;
                valuePtr[Int(outerIdx)].withUnsafeMutableBufferPointer( { innerValuePtr -> Void in
                    dispatch_apply(ySize, the_queue) { innerOuterIdx -> Void in
                        let x = innerOuterIdx;
                        if(innerValuePtr[x].grey >= seperateValue){
                            innerValuePtr[x] = PixelData(grey: 255);
                        }else{
                            innerValuePtr[x] = PixelData(grey: 0);
                        }
                    }
                } );
            }
        } )
    }
    
    func doParralel( functor : (inout pxl : PixelData, x : Int, y : Int) -> Void ){
//        if(true){
            let the_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
            let xS = m_xSize;
            let yS = m_ySize;
            m_img.m_val.withUnsafeMutableBufferPointer( { valuePtr -> Void in
                dispatch_apply(xS, the_queue) { outerIdx -> Void in
                    let x = outerIdx;
                    let ySize = yS;
                    valuePtr[Int(outerIdx)].withUnsafeMutableBufferPointer( { innerValuePtr -> Void in
                        dispatch_apply(ySize, the_queue) { innerOuterIdx -> Void in
                            let y = innerOuterIdx;
                            functor(pxl: &innerValuePtr[y],x: x,y: y);
                        }
                    } );
                }
            } )
//        }else{
//            for x in 0 ..< m_xSize {
//                for y in 0 ..< m_ySize {
//                    functor(pxl: &m_img.m_val[x][y], x: x, y: y);
//                }
//            }
//        }
    }
    
    func generateMatchingBowStrings(with template : Image, bows : [BowString], withScalingFactor hasFactor : Bool ) -> [BowStringWithData] {
        var result : [BowStringWithData] = [];
        let factor = 2;
        if(hasFactor){
//            let the_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
            let size = bows.count;
//            bows.withUnsafeMutableBufferPointer( { valuePtr -> Void in
//                dispatch_apply(size , the_queue) { i -> Void in
            for i in 0 ..< size {
                let x = bows[i].m_xb * factor;
                let y = bows[i].m_yb * factor;
                let orgX = bows[i].m_xb;
                let orgY = bows[i].m_yb;
                let newBowUpperRow = BowStringWithData(xb: x,     yb: y, len: bows[i].m_len * factor);
                let newBowLowerRow = BowStringWithData(xb: x + 1, yb: y, len: bows[i].m_len * factor);
                for ySmall in 0 ..< bows[i].m_len {
                    var resultVal : Int = 0;
                    let xTempSize = template.m_xSize;
                    let yTempSize = template.m_ySize;
                    for i in 0 ..< xTempSize {
                        for j in 0 ..< yTempSize {
                            let newX : Int = i - xTempSize / 2 + orgX;
                            let newY : Int = j - yTempSize / 2 + orgY + ySmall;
                            if(newX >= 0 && newX < self.m_xSize && newY >= 0 && newY < self.m_ySize){
                                resultVal += abs(Int(template.m_img.m_val[i][j].grey) - Int(self.m_img.m_val[newX][newY].grey))
                            }else{
                                resultVal += 128;
                            }
                        }
                    }
                    let pxlData = PixelData(grey: 255 - UInt8(resultVal / (template.m_xSize * template.m_ySize)));
                    for yS in 0 ..< factor {
                        newBowUpperRow.m_data[ySmall * factor + yS] = pxlData;
                        newBowLowerRow.m_data[ySmall * factor + yS] = pxlData;
                    }
                }
                result.append(newBowUpperRow);
                result.append(newBowLowerRow);
            }
//            });
        }else{
            for bow in bows {
                let orgX = bow.m_xb;
                let orgY = bow.m_yb;
                let newBowRow = BowStringWithData(xb: orgX,     yb: orgY, len: bow.m_len * factor);
                for ySmall in 0 ..< bow.m_len {
                    var resultVal : Int = 0;
                    let xTempSize = template.m_xSize;
                    let yTempSize = template.m_ySize;
                    for i in 0 ..< xTempSize {
                        for j in 0 ..< yTempSize {
                            let newX : Int = i - xTempSize / 2 + orgX;
                            let newY : Int = j - yTempSize / 2 + orgY + ySmall;
                            if(newX >= 0 && newX < m_xSize && newY >= 0 && newY < m_ySize){
                                resultVal += abs(Int(template.m_img.m_val[i][j].grey) - Int(m_img.m_val[newX][newY].grey))
                            }else{
                                resultVal += 128;
                            }
                        }
                    }
                    let pxlData = PixelData(grey: 255 - UInt8(resultVal / (template.m_xSize * template.m_ySize)));
                    for yS in 0 ..< factor {2
                        newBowRow.m_data[ySmall * factor + yS] = pxlData;
                    }
                }
                result.append(newBowRow);
            }

        }
        return result;
    }

    func generateMatchingImage(with template : Image) -> Image {
        normTo(seperateValue: 148);
        globalAppRef?.showImage(self);
        print("temp: \(template.m_xSize) \(template.m_ySize)");
        globalAppRef?.showImage(template);
        let xSize = m_xSize;
        let ySize = m_ySize;
        var result = Image(xSize: xSize, ySize: ySize);
        func forPar(inout pxl : PixelData, x : Int, y : Int){
            var resultVal : Int = 0;
            for i in 0 ..< template.m_xSize {
                for j in 0 ..< template.m_ySize {
                    let newX : Int = i - template.m_xSize / 2 + x;
                    let newY : Int = j - template.m_ySize / 2 + y;
                    if(newX >= 0 && newX < xSize && newY >= 0 && newY < ySize){
                        resultVal += abs(Int(template.m_img.m_val[i][j].grey) - Int(m_img.m_val[newX][newY].grey))
                    }else{
                        resultVal += 128;
                    }
                }
            }
            pxl = PixelData(grey: 255 - UInt8(resultVal / (template.m_xSize * template.m_ySize)));
        }
        let sw = StopWatch(name: "doPar");
        result.doParralel(forPar);
        sw.printElapsedTime();
        return result;
    }
    
    func getHistogram() -> [Int] {
        var histo = [Int](count: 256, repeatedValue: 0);
        for line in m_img.m_val {
            for ele in line {
                histo[Int(ele.grey)] += 1;
            }
        }
        return histo;
    }

    func getGreyValueInHistoForBlack( val : Double ) -> Int {
        let histo = getHistogram();
        print("Histo: \(histo)");
        let elements = Double(m_xSize * m_ySize);
        var eleTill = 0;
        var newHisto : Int = 0;
        for i in (0 ... 255).reverse() {
            eleTill += histo[i];
            if(Double(eleTill) / elements > 1.0 - val){
                newHisto = i;
                break;
            }
        }
        return newHisto;
    }
    
    func getPyramideImage(level  level : Int) -> [Image] {
        var pyramide = [self, imagePyramide()];
        if(level > 1){
            for _ in 2 ... level + 1 {
                pyramide.append(pyramide.last!.imagePyramide());
            }
        }
        return pyramide;
    }
    
    func imagePyramide() -> Image {
        let result = Image(xSize: m_xSize / 2, ySize: m_ySize / 2);
        for x in 0.stride(to: m_xSize - 1, by: 2) {
//        for var x = 0; x < m_xSize - 1; x+=2 {
            for y in 0.stride(to: m_ySize - 1, by: 2) {
//            for var y = 0; y < m_ySize - 1; y+=2 {
                let val = UInt8((Int(m_img.m_val[x][y].grey) + Int(m_img.m_val[x+1][y].grey) + Int(m_img.m_val[x][y+1].grey) + Int(m_img.m_val[x+1][y+1].grey)) / 4);
                result.m_img.m_val[x/2][y/2] = PixelData(grey: val);
            }
        }
        return result;
    }
    
    func setValAt(x: Int, y: Int, val: PixelData) {
        if(x > 0 && x < m_xSize && y > 0 && y < m_ySize){
            m_img.m_val[x][y] = val;
        }else{
            let nX = x < 0 ? -x : x >= m_xSize ? 2 * m_xSize - x - 1 : x;
            let nY = y < 0 ? -y : y >= m_ySize ? 2 * m_ySize - y - 1 : y;
            m_img.m_val[nX][nY] = val;
        }
    }
    
    func setDirectValAt(x x: Int, y: Int, val: PixelData) {
        m_img.m_val[x][y] = val;
    }
    
    func setFor( bowStrings : [BowString], to data : PixelData) {
        for bow in bowStrings {
            for i in bow.m_yb ..< bow.m_yb + bow.m_len {
                m_img.m_val[bow.m_xb][i] = PixelData(pxl: data)
            }
        }
    }
    
    func setFor( bowStrings : [BowStringWithData]) {
        for bow in bowStrings {
            for y in 0 ..< bow.m_len {
                m_img.m_val[bow.m_xb][bow.m_yb + y] = bow.getPixelData(y: y);
            }
        }
    }
    
    func avgFilter(windowSize : Int){
        let sw = StopWatch(name: "Copy ImageData");
        sw.printEleAndReset("Avg Filter");
        let windowElements = windowSize * windowSize;
        let windowSizeHalf = windowSize / 2;
        let oldImg : ImageData = ImageData(cpy: self.m_img);
        
        print("Size: \(m_xSize), \(m_ySize)");
        let the_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        m_img.m_val.withUnsafeMutableBufferPointer( { valuePtr -> Void in
            dispatch_apply(m_xSize, the_queue) { x -> Void in
                
                var xVals = [Int](count: windowSize ,repeatedValue: 0);
                var sum : UInt8 = 0;
                var t = 0;
                for k in -windowSizeHalf ... windowSizeHalf {
                    let pX = k+x;
                    xVals[t] = pX < 0 ? -pX : pX >= self.m_xSize ? 2 * self.m_xSize - pX - 2 : pX;
                    t += 1;
                }
                for t2 in 0 ..< windowSize {
                    for l in -windowSizeHalf ... windowSizeHalf {
                        sum += oldImg.m_val[xVals[t2]][abs(l)].grey;
                    }
                }
                for y in 0 ..< self.m_ySize {
                    self.setValAt(x, y: y, val: PixelData(grey: UInt8(Int(sum) / windowElements)));
//                    valuePtr[x][y] = PixelData(grey: UInt8(Int(sum) / windowElements));
                    let pY =  y-windowSizeHalf;
                    let pY2 = y+windowSizeHalf+1;
                    let nY = pY < 0 ? -pY : pY >= self.m_ySize ? 2 * self.m_ySize - pY - 2 : pY;
                    let nY2 = pY2 < 0 ? -pY2 : pY2 >= self.m_ySize ? 2 * self.m_ySize - pY2 - 2 : pY2;
                    for t2 in 0 ..< windowSize {
                        sum -= oldImg.m_val[xVals[t2]][nY].grey;
                        sum += oldImg.m_val[xVals[t2]][nY2].grey;
                    }
                }
                sum = 0;
            }
            }
        );
        
        sw.printElapsedTime();
//        for var y = 0; y < m_ySize; y++ {
//            for var x = 0; x < m_xSize; x++ {
//                m_img.m_val[x][y] = max(0, min(255, m_img.m_val[x][y]));
//            }
//        }
    }
    
    func findRegionsGreyValue(greyVal : UInt8, functor : (imageVal : UInt8, greyVal : UInt8) -> Bool) -> [BowString] {
        var foreground : [BowString] = [];
        var isActiv = false;
        var len = 0;
        var actBow = BowString(xb: 0, yb: 0, len: 0);
        for x in 0 ..< m_xSize {
            var isInForeGround = false;
            for y in 0 ..< m_ySize {
                isInForeGround = functor(imageVal: m_img.m_val[x][y].grey, greyVal: greyVal);
//                let isInForeGround = functor(lhs: m_img.m_val[x][y].grey, rhs: greyVal);
                if(isInForeGround && isActiv){
                    len += 1;
                    // during the bow
                }else if(isInForeGround && !isActiv){
                    // starts the bow
                    len = 0;
                    isActiv = true;
                    actBow = BowString(xb: x, yb: y, len: 0);
                }else if(!isInForeGround && isActiv){
                    // end of bow
                    len += 1;
                    isActiv = false;
                    actBow.m_len = len;
                    foreground.append(actBow);
                }
            }
            if(isActiv){
                // end of bow
                len += 1;
                isActiv = false;
                actBow.m_len = len;
                foreground.append(actBow);
            }
        }
        return foreground;
    }
    
    func findRegionsGreyValueWithData(greyVal : UInt8, functor : (imageVal : UInt8, greyVal : UInt8) -> Bool) -> [BowStringWithData] {
        var foreground : [BowStringWithData] = [];
        var isActiv = false;
        var len = 0;
        var actBow = BowStringWithData(xb: 0, yb: 0, len: 0);
        for x in 0 ..< m_xSize {
            var isInForeGround = false;
            for y in 0 ..< m_ySize {
                let actGreyValue = m_img.m_val[x][y];
                isInForeGround = functor(imageVal: actGreyValue.grey, greyVal: greyVal);
                //                let isInForeGround = functor(lhs: m_img.m_val[x][y].grey, rhs: greyVal);
                if(isInForeGround && isActiv){
                    len += 1;
                    actBow.m_data.append(actGreyValue);
                    // during the bow
                }else if(isInForeGround && !isActiv){
                    // starts the bow
                    len = 0;
                    isActiv = true;
                    actBow = BowStringWithData(xb: x, yb: y, len: 0);
                    actBow.m_data.append(actGreyValue);
                }else if(!isInForeGround && isActiv){
                    // end of bow
                    len += 1;
                    isActiv = false;
                    actBow.m_len = len;
                    assert(actBow.m_len == actBow.m_data.count, "The insertion did not work well");
                    foreground.append(actBow);
                }
            }
            if(isActiv){
                // end of bow
                len += 1;
                isActiv = false;
                actBow.m_len = len;
                foreground.append(actBow);
            }
        }
        return foreground;
    }
}