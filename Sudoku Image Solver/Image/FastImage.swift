//
//  FastImage.swift
//  Sudoku Image Solver
//
//  Created by Max on 13.04.16.
//  Copyright © 2016 Maximilian Denninger. All rights reserved.
//

import Foundation
import SpriteKit


class GreyImage {
    
    var m_xSize = 0;
    var m_ySize = 0;
    
    var m_data : GreyImageData;
    
    init(file : String){
        let img = NSImage(contentsOfFile: file);
        var image : NSImage;
        if(img != nil){
            image = img!;
        }else{
            assert(true, "There is no NSImage!");
            image = NSImage();
            m_xSize = 0;
            m_ySize = 0;
            m_data = GreyImageData(xSize: m_xSize, ySize: m_ySize);
            return;
        }
        let bmp = image.representations[0] as! NSBitmapImageRep
        m_xSize = bmp.pixelsHigh;
        m_ySize = bmp.pixelsWide;
        m_data = GreyImageData(xSize: m_xSize, ySize: m_ySize, img: image);
    }
    
    func findNumbers(inout grid : Grid){
        if(m_xSize == 0 && m_ySize == 0){
            printError("The image has to be inited before it is used!");
            return;
        }
        print("Start")
        for line in m_data.m_val {
            for var ele in line { // up 128 to 255, else to 0
                ele.g = (ele.g >> 7) * 255;
            }
        }
        
        var bows = getBowStrings()

    }
    
    func getBowStrings() -> BowSet {
        
    }
    
    
}