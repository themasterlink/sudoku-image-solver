//
//  BowString.swift
//  ImageUnderstanding
//
//  Created by Max on 26.06.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

func ==(lhs : BowString, rhs : BowString) -> Bool {
    return lhs.m_yb == rhs.m_yb && lhs.m_xb == rhs.m_xb && lhs.m_len == rhs.m_len;
}

func !=(lhs : BowString, rhs : BowString) -> Bool {
    return !(lhs == rhs);
}

func >=(lhs : BowString, rhs : BowString) -> Bool {
    let equal = (lhs.m_yb == rhs.m_yb && lhs.m_xb == rhs.m_xb && lhs.m_len == rhs.m_len);
    var smaller = false;
    if(lhs.m_xb == rhs.m_xb){ // same row
        smaller = lhs.m_yb > rhs.m_yb;
    }else{ // different row
        smaller = lhs.m_xb > rhs.m_xb;
    }
    return smaller || equal;
}

func <(lhs : BowString, rhs : BowString) -> Bool {
    var smaller = false;
    if(lhs.m_xb == rhs.m_xb){ // same row
        smaller = lhs.m_yb < rhs.m_yb;
    }else{ // different row
        smaller = lhs.m_xb < rhs.m_xb;
    }
    return smaller;
}

func >(lhs : BowString, rhs : BowString) -> Bool {
    var bigger = false;
    if(lhs.m_xb == rhs.m_xb){ // same row
        bigger = lhs.m_yb > rhs.m_yb;
    }else{ // different row
        bigger = lhs.m_xb > rhs.m_xb;
    }
    return bigger;
}

func <=(lhs : BowString, rhs : BowString) -> Bool {
    let equal = (lhs.m_yb == rhs.m_yb && lhs.m_xb == rhs.m_xb && lhs.m_len == rhs.m_len);
    var smaller = false;
    if(lhs.m_xb == rhs.m_xb){ // same row
        smaller = lhs.m_yb < rhs.m_yb;
    }else{ // different row
        smaller = lhs.m_xb < rhs.m_xb;
    }
    return smaller || equal;
}




class BowString : CustomStringConvertible {
    
    var m_yb : Int;
    var m_xb : Int;
    var m_len : Int;
    
    init(xb : Int, yb : Int, len : Int){
        m_xb = xb;
        m_yb = yb;
        m_len = len;
    }

    func intersecWith(rhs : BowString) -> Bool {
        if(m_xb == rhs.m_xb){
            if(m_yb < rhs.m_yb){
                return m_yb + m_len - 1 >= rhs.m_yb;
            }else{
                return rhs.m_yb + rhs.m_len - 1 >= m_yb;
            }
        }
        return false;
    }
    
    func touches(rhs : BowString) -> Bool {
        if(abs(rhs.m_xb - m_xb) < 2){
            if(m_xb == rhs.m_xb){
                if(m_yb < rhs.m_yb){
                    return m_yb + m_len - 1 == rhs.m_yb;
                }else{
                    return rhs.m_yb + rhs.m_len - 1 == m_yb;
                }
            }else{
                if(m_yb < rhs.m_yb){
                    return m_yb + m_len - 1 >= rhs.m_yb;
                }else{
                    return rhs.m_yb + rhs.m_len - 1 >= m_yb;
                }
            }
        }
        return false;
    }
    
    func touches(rhs : BowString, withClosing dist : Int) -> Bool {
        if(abs(rhs.m_xb - m_xb) <= dist){
            if(m_yb < rhs.m_yb){
                return m_yb + m_len - 1 + dist >= rhs.m_yb;
            }else{
                return rhs.m_yb + rhs.m_len - 1 + dist >= m_yb;
            }
        }
        return false;
    }

    
    var description : String {
        get{
            return "Bow from [\(m_xb), \(m_yb)] with len: \(m_len)";
        }
    }
    
    
}