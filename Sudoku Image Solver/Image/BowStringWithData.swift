//
//  BowStringWithData.swift
//  Sudoku Image Solver
//
//  Created by Max on 26.09.15.
//  Copyright © 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

func getGreyValueInHistoForBlackInPyr(val : Double, forBows reg : [BowStringWithData]) -> UInt8 {
    var histo = [Int](count: 256, repeatedValue: 0);
    var elements = 0;
    for bow in reg {
        for y in 0 ..< bow.m_len {
            histo[Int(bow.getPixelData(y: y).grey)] += 1;
        }
        elements += bow.m_len;
    }
    var eleTill = 0;
    var newHisto : UInt8 = 0;
    for i in (0 ... 255).reverse() {
        eleTill += histo[i];
        if(Double(eleTill) / Double(elements) > 1.0 - val){
            newHisto = UInt8(i);
            break;
        }
    }
    return newHisto;
}

func highestValueInRegion(forBows reg : [BowStringWithData]) -> (x: Int, y: Int, highestValue : UInt8) {
    var result : (x: Int, y: Int, highestValue : UInt8) = (0,0,0);
    for bow in reg {
        for y in 0 ..< bow.m_len {
            if(result.highestValue < bow.getPixelData(y: y).grey){
                result.highestValue = bow.getPixelData(y: y).grey;
                result.x = bow.m_xb;
                result.y = bow.m_yb + y;
            }
        }
    }
    return result;
}

func seperateRegions(forBows reg : [BowStringWithData], withClosing dist : Int) -> [[BowStringWithData]] {
    if(reg.count > 0){
        var regCopy = [BowStringWithData](count: reg.count, repeatedValue: BowStringWithData(xb: 0, yb: 0, len: 0));
        for i in 0 ..< reg.count {
            regCopy[i] = BowStringWithData(xb: reg[i].m_xb, yb: reg[i].m_yb, len: reg[i].m_len);
            for j in 0 ..< reg[i].m_len {
                regCopy[i].m_data[j] = reg[i].m_data[j];
            }
        }
        var result : [[BowStringWithData]] = [];
        while(regCopy.count > 0){
            let actBow = regCopy[0];
            var actRegion : [BowStringWithData] = [actBow];
            var indices : [Int] = [];
            indices.append(0);
            for i in 1 ..< regCopy.count {
                if(actBow.touches(regCopy[i], withClosing: dist)){
                    actRegion.append(regCopy[i]);
                    indices.append(i);
                }
            }
            var saveOfCountNumber = regCopy.count;
            for k in (0 ..< indices.count).reverse() {
                regCopy.removeAtIndex(indices[k]);
            }
            while(saveOfCountNumber > regCopy.count){
                indices.removeAll(keepCapacity: true);
                saveOfCountNumber = regCopy.count;
                for i in 1 ..< regCopy.count {
                    for bows in actRegion {
                        if(bows.touches(regCopy[i], withClosing: dist)){
                            actRegion.append(regCopy[i]);
                            indices.append(i);
                            break;
                        }
                    }
                }
                for k in (0 ..< indices.count).reverse() {
                    regCopy.removeAtIndex(indices[k]);
                }
            }
            result.append(actRegion);
        }
        return result;
    }
    return [];
}

func getCenterOfRegion(forBows reg : [BowStringWithData]) -> (x: Int, y : Int) {
    var xD = 0.0;
    var yD = 0.0;
    var elemCount = 0;
    for bow in reg {
//        var valCount = 0.0;
        for x in 0 ..< bow.m_len {
//            let val = Double(bow.getPixelData(cb: x).grey);
            let val = 1.0;
            xD += Double(x + bow.m_yb) * val;
            yD += Double(bow.m_xb) * val;
            
            elemCount += 1;
//            valCount += val;
        }
//        elemCount += Int(valCount);
    }
    xD /= Double(elemCount);
    yD /= Double(elemCount);
    return (Int(floor(xD)), Int(floor(yD)));
}

func findInRegionsGreyValueWithData(greyVal : UInt8, forBows reg : [BowStringWithData], functor : (imageVal : UInt8, greyVal : UInt8) -> Bool) -> [BowStringWithData] {
    var foreground : [BowStringWithData] = [];
    var isActiv = false;
    var len = 0;
    var actBow = BowStringWithData(xb: 0, yb: 0, len: 0);
    for bow in reg {
        var isInForeGround = false;
        for y in 0 ..< bow.m_len {
            let actGreyValue = bow.getPixelData(y: y);
            isInForeGround = functor(imageVal: actGreyValue.grey, greyVal: greyVal);
            //                let isInForeGround = functor(lhs: m_img.m_val[x][y].grey, rhs: greyVal);
            if(isInForeGround && isActiv){
                len += 1;
                actBow.m_data.append(actGreyValue);
                // during the bow
            }else if(isInForeGround && !isActiv){
                // starts the bow
                len = 0;
                isActiv = true;
                actBow = BowStringWithData(xb: bow.m_xb, yb: bow.m_yb + y, len: 0);
                actBow.m_data.append(actGreyValue);
            }else if(!isInForeGround && isActiv){
                // end of bow
                len += 1;
                isActiv = false;
                actBow.m_len = len;
                assert(actBow.m_len == actBow.m_data.count, "The insertion did not work well");
                foreground.append(actBow);
            }
        }
        if(isActiv){
            // end of bow
            len += 1;
            isActiv = false;
            actBow.m_len = len;
            foreground.append(actBow);
        }
    }
    return foreground;
}

class BowStringWithData : BowString {
    
    override init(xb : Int, yb : Int, len : Int){
        m_data = [PixelData](count: len, repeatedValue: PixelData());
        super.init(xb: xb, yb: yb, len: len);
    }
    
    init(cpy : BowStringWithData){
        m_data = [PixelData](count: cpy.m_len, repeatedValue: PixelData(a: 0, r: 0, g: 0, b: 0));
        super.init(xb: cpy.m_xb, yb: cpy.m_yb, len: cpy.m_len)
        for y in 0 ..< m_len {
            m_data[y] = cpy.m_data[y];
        }
    }
    
    func setNewLength(len : Int) {
        self.m_len = len;
        m_data = [PixelData](count: len, repeatedValue: PixelData());
    }
    
    func maxContrast() {
        var min : UInt8 = 255;
        var max : UInt8 = 0;
        for y in 0 ..< m_len {
            let grey = getPixelData(y: y).grey;
            if(grey < min){
                min = grey;
            }
            if(grey > max){
                max = grey;
            }
        }
        if(max - min > 0){
            let diff = 255 / (max - min);
            for y in 0 ..< m_len {
                m_data[y] = PixelData(grey: (m_data[y].grey - min) * diff);
            }
        }
    }
    
    func getGreyValueInHistoForBlack(val : Double) -> Int {
        let histo = getHistogram();
        var eleTill = 0;
        var newHisto : Int = 0;
        for i in (0 ... 255).reverse() {
            eleTill += histo[i];
            if(Double(eleTill) / Double(m_len) > 1.0 - val){
                newHisto = i;
                break;
            }
        }
        return newHisto;
    }

    func setWith(bow : BowStringWithData) {
        m_len = bow.m_len;
        m_yb = bow.m_yb;
        m_xb = bow.m_xb;
        m_data = [PixelData](count: m_len, repeatedValue: PixelData());
        for i in 0 ..< m_len {
            m_data[i] = bow.m_data[i];
        }
    }
    
    func getHistogram() -> [Int] {
        var histo = [Int](count: 256, repeatedValue: 0);
        for y in 0 ..< m_len {
            histo[Int(getPixelData(y: y).grey)] += 1;
        }
        return histo;
    }
    
    
    func getPixelData(y y : Int) -> PixelData {
        return m_data[y];
    }
    
    var m_data : [PixelData];
    
}