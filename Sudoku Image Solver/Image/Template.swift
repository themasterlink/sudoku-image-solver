//
//  Template.swift
//  Sudoku Image Solver
//
//  Created by Max on 08.09.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation


class Template {
    
    var m_img : Image;
    
    init(file : String){
        m_img = Image(file: file);
    }
}