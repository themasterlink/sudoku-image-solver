//
//  Solver.swift
//  Sudoku
//
//  Created by Max on 04.09.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

class Solver{
    
    var m_grid = Grid();
    
    func updateStep(inout forGrid grid : Grid) -> Bool {
        var somethingChanged = false;
        for x in 0 ..< 9 {
            for y in 0 ..< 9 {
                let newChange = grid.updateFor(x, y: y);
                if(newChange){
                    somethingChanged = true;
                }
            }
        }
        return somethingChanged;
    }
    
    func findSingleElementsAndSetThem(inout forGrid grid : Grid){
        for x in 0 ..< 9 {
            for y in 0 ..< 9 {
                let ele = grid[x,y];
                if(!ele.m_isDefined){
                    grid.updateFor(x, y: y);
                    if(ele.m_candidates.count == 1){
                        ele.m_field = ele.m_candidates[0];
                        ele.m_candidates = [];
                    }
                }
            }
        }
    }
    
    func findElementWithMinimumOfCandidates(inout forGrid grid : Grid) -> (x: Int, y : Int) {
        var ret : (x : Int, y : Int) = (-1,-1);
        var minCandidates = 10;
        for x in 0 ..< 9 {
            for y in 0 ..< 9 {
                if(minCandidates > grid[x,y].m_candidates.count && !grid[x,y].m_isDefined){
                    ret = (x,y);
                    minCandidates = grid[x,y].m_candidates.count;
                }
            }
        }
        return ret;
    }
    
    init(grid : Grid){
        m_grid = grid;
        let sw = StopWatch(name: "Grid solved in");
        recursiveSolver(forGrid: &m_grid);
        sw.printElapsedTime();
        m_grid.printGrid();
        
    }
    
    init(filePath : String){
        m_grid.readFile(filePath);
        let sw = StopWatch();
        recursiveSolver(forGrid: &m_grid);
        //m_grid.printGridWithCandidates();
        sw.printElapsedTime();
        //m_grid.printControllGrids();
    }
    
    func recursiveSolver(inout forGrid grid : Grid) -> Bool {
        if(!grid.isSolved()){
            var doItAgain = true;
            while(doItAgain){
                doItAgain = updateStep(forGrid: &grid);
                findSingleElementsAndSetThem(forGrid: &grid);
                //grid.printGridWithCandidates();
                if(!grid.isStillSolveabel()){
                    return false;
                }
            }
            if(!grid.isSolved()){
                updateStep(forGrid: &grid);
                let nextCandPos = findElementWithMinimumOfCandidates(forGrid: &grid);
                var newGrid = Grid(grid: grid);
                var candidates = [Int](newGrid[nextCandPos.x, nextCandPos.y].m_candidates);
                repeat{
                    let nextUsedCand = candidates[0];
                    newGrid[nextCandPos.x, nextCandPos.y].m_field = nextUsedCand;
                    newGrid[nextCandPos.x, nextCandPos.y].m_candidates = [];
                    let solved = recursiveSolver(forGrid: &newGrid);
                    if solved {
                        grid = Grid(grid: newGrid);
                        return true;
                    }else{
                        candidates.removeAtIndex(0);
                        newGrid = Grid(grid: grid);
                    }
                }while(candidates.count > 0)
                
            }else{
                return true;
            }
        }else{
            return true;
        }
        return false;
    }
}