//
//  Grid.swift
//  Sudoku
//
//  Created by Max on 04.09.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Foundation


class Element{
   
    var m_field : Int = 0;
    var m_candidates : [Int] = [];
    
    var m_isDefined : Bool {
        get{
            return m_field > 0 && m_field < 10;
        }
    }
    
    init(){
        makeAllCandidatesPossible();
    }
    
    init(cpy : Element){
        m_field = cpy.m_field;
        for i in 0 ..< cpy.m_candidates.count {
            m_candidates.append(cpy.m_candidates[i]);
        }
    }
    
    init(fieldValue : Int){
        m_field = fieldValue;
    }
    
    func hasCandidate(val : Int) -> Bool {
        for candidate in m_candidates {
            if(candidate == val){
                return true
            }
        }
        return false;
    }
    
    func makeAllCandidatesPossible(){
        m_candidates = [1,2,3,4,5,6,7,8,9];
    }
    func removeCandidate(val : Int){
        for i in 0 ..< m_candidates.count {
            if(val == m_candidates[i]){
                m_candidates.removeAtIndex(i);
                return;
            }
        }
    }
    
}

class Grid {
    
    var m_data = [[Element]](count: 9, repeatedValue: [Element](count: 9, repeatedValue: Element()));
    
    init(){
    }
    
    init(grid : Grid){
        for x in 0 ..< 9 {
            for y in 0 ..< 9 {
                m_data[x][y] = Element(cpy: grid[x,y]);
            }
        }
    }
    
    func initWithExample(){
        var elements : [Element] = [];
        for i in 1 ... 9 {
            elements.append(Element(fieldValue: i));
        }
        
    }
    
    func printGrid(){
        for rows in m_data {
            for ele in rows {
                if(ele.m_isDefined){
                    print("\(ele.m_field) ", terminator: "");
                }else{
                    print("  ", terminator: "");
                }
            }
            print("");
        }
    }
    
    func printControllGrids(){
        for i in 1 ... 9 {
        for rows in m_data {
            for ele in rows {
                if(ele.m_isDefined && ele.m_field == i){
                    print("\(ele.m_field) ", terminator: "");
                }else{
                    print("  ", terminator: "");
                }
            }
            print("");
        }
        }
    }
    
    func printGridWithCandidates(){
        print("---------------------------------------------------------------");
        for x in 0 ..< 9 {
            for i in 0 ..< 3 {
                for y in 0 ..< 9 {
                    let ele = m_data[x][y];
                    print("|", terminator: "");
                    for j in 1 ..< 4 {
                        if(ele.m_isDefined){
                            if((i * 3 + j) != 5){
                                print("  ", terminator: "");
                            }else{
                                print("\(ele.m_field) ", terminator: "");
                            }
                        }else{
                            if(ele.hasCandidate(i * 3 + j)){
                                print("\(i * 3 + j) ", terminator: "");
                            }else{
                                print("  ", terminator: "");
                            }
                        }
                    }
                }
                print("|", terminator: "");
                print("");
            }
            print("---------------------------------------------------------------");
        }
    }
    
    func readFile(filePath : String){
        var error:NSError?
        var text: String?
        do {
            text = try String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding)
        } catch let error1 as NSError {
            error = error1
            text = nil
        };
        if let theError = error {
            print("\(theError.localizedDescription)");
            return;
        }
        var lines = text!.componentsSeparatedByString("\n");
        print("lines: \(lines.count)");
        assert(lines.count == 9, "Text must have 9 lines!");
        for x in 0 ..< 9 {
            var values = lines[x].componentsSeparatedByString(" ");
            for y in 0 ..< 9 {
                let val = Int(values[y]);
                if(val != nil){
                    m_data[x][y] = Element(fieldValue: val!);
                }else{
                    m_data[x][y] = Element();
                    m_data[x][y].makeAllCandidatesPossible();
                }
            }
        }
    }
    
    func isSolved() -> Bool {
        for x in 0 ..< 9 {
            for y in 0 ..< 9 {
                if(!m_data[x][y].m_isDefined){
                    return false;
                }
            }
        }
        return true;
    }
    
    func updateFor(x : Int, y : Int) -> Bool {
        var somethingChanged = false;
        let ele = m_data[x][y];
        if(!ele.m_isDefined){
            for rowX in 0 ..< 9 {
                if(self[rowX, y].m_isDefined && rowX != x && ele.hasCandidate(self[rowX, y].m_field)){
                    ele.removeCandidate(self[rowX, y].m_field);
                    somethingChanged = true;
                }
            }
            for colY in 0 ..< 9 {
                if(self[x, colY].m_isDefined && colY != y && ele.hasCandidate(self[x, colY].m_field)){
                    ele.removeCandidate(self[x, colY].m_field);
                    somethingChanged = true;
                }
            }
            for i in 0 ..< 3 {
                for j in 0 ..< 3 {
                    let newX = i - (x % 3) + x;
                    let newY = j - (y % 3) + y;
                    if(self.isValid(newX, y: newY)){
                        if(self[newX, newY].m_isDefined && newX != x && newY != y && ele.hasCandidate(self[newX, newY].m_field)){
                            ele.removeCandidate(self[newX, newY].m_field);
                            somethingChanged = true;
                        }
                    }
                }
            }
        }
        return somethingChanged;
    }
    
    func isStillSolveabel() -> Bool {
        
        for x in 0 ..< 9 {
            for y in 0 ..< 9 {
                if(m_data[x][y].m_candidates.count == 0 && !m_data[x][y].m_isDefined){
                    return false;
                }
            }
        }
        return true;
    }
    
    subscript(x: Int, y : Int) -> Element {
        get {
        return m_data[x][y];
        }
        set(newVal){
            m_data[x][y] = newVal;
        }
    }
    
    func isValid(x : Int, y : Int) -> Bool {
        return x >= 0 && x < 9 && y >= 0 && y < 9;
    }
}