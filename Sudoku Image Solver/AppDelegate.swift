//
//  AppDelegate.swift
//  Sudoku Image Solver
//
//  Created by Max on 07.09.15.
//  Copyright (c) 2015 Maximilian Denninger. All rights reserved.
//

import Cocoa
//import AVFoundation

let isDebug = true;

var globalAppRef : AppDelegate? = nil;

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!
    var windowPosX = 20.0;
    var windowPosY = 20.0;
    
    
    func showImage(img : Image){
        let nsImgView = NSImageView();
        let sizeX = Double(img.m_xSize > 40 ? img.m_xSize : 40);
        let sizeY = Double(img.m_ySize > 40 ? img.m_ySize : 40);
        nsImgView.setFrameSize(NSSize(width: sizeX, height: sizeY));
        nsImgView.image = img.m_img.getNSImage();
        nsImgView.allowsCutCopyPaste = true;
        let newWindow = NSWindow();
        newWindow.contentView = nsImgView;
        newWindow.setFrame(NSRect(x: windowPosX, y: windowPosY, width: sizeX, height: sizeY), display: true);
        windowPosX += 20;
        windowPosY += 20;
        window.addChildWindow(newWindow, ordered: NSWindowOrderingMode.Above);
    }

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        globalAppRef = self;
        // Insert code here to initialize your application
//        system("pwd");
        var name = "/Users/Max/Documents/XCode_Projects/Bitbucket/Sudoku Image Solver/pics/Sudoku3.jpg"//"/Users/test/Desktop/Sudoku3.jpg"//NSString(data: NSFileHandle.fileHandleWithStandardInput().availableData, encoding:NSUTF8StringEncoding)
        var img = Image(file: name);
//        print("\n\n\(name as! String)\n\n");
        // "/Users/Max/Documents/XCode_Projects/Bitbucket/Sudoku Image Solver/pics/Sudoku3.jpg"
//        showImage(img.getPyramideImage(level: 2)[1]);
        printInfo("img size: \(img.m_xSize), \(img.m_ySize)");
        //var img = NSImage(contentsOfFile: "/Users/Max/Documents/XCode_Projects/Bitbucket/Sudoku/Sudoku/Sudoku.jpg");
        func lower(lhs : UInt8, rhs : UInt8) -> Bool {
            return lhs > rhs;
        }
        
        var grid = Grid();
        let sw = StopWatch(name: "Find number")
        img.findFastNumbers(&grid);
        sw.printElapsedTime();
        let _ = Solver(grid: grid);
        
        
        var fastImg = GreyImage(file: name)
        fastImg.findNumbers(&grid);
        //img = img.generateTemplate(temp);
//        var max : UInt8 = 0;
//        var maxX = 0;
//        var maxY = 0;
//        for var x = 1; x < img.m_xSize - 1; x++ {
//            for var y = 1; y < img.m_ySize - 1; y++ {
//                if(max < img.m_img.m_val[x][y].grey){
//                    max = img.m_img.m_val[x][y].grey
//                    maxX = x
//                    maxY = y
//                }
//            }
//        }
        
        
//        for var x = 1; x < img.m_xSize - 1; x++ {
//            for var y = 1; y < img.m_ySize - 1; y++ {
//                if(img.m_img.m_val[x][y].grey > UInt8(newHisto)){
//                    img.stampAt(x: x, y: y, pxl: PixelData(a: 255, r: 255, g: 0, b: 0));
//                }
//            }
//        }
//        img.stampAt(x: maxX, y: maxY, pxl: PixelData(a: 255, r: 255, g: 0, b: 0));
//        println("\(max)");
//        var bows = img.findRegionsGreyValue(max - 10 , functor: lower);
        
//        img.setFor(bows, to: PixelData(a: 255, r: 0, g: 255, b: 0));
//        println("max: \(maxX), \(maxY)");
        //img.avgFilter(5);
        print("end");
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "Max.Sudoku_Image_Solver" in the user's Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.ApplicationSupportDirectory, inDomains: .UserDomainMask)
        let appSupportURL = urls[urls.count - 1] 
        return appSupportURL.URLByAppendingPathComponent("Max.Sudoku_Image_Solver")
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Sudoku_Image_Solver", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.) This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        let fileManager = NSFileManager.defaultManager()
        var shouldFail = false
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."

        // Make sure the application files directory is there
        let propertiesOpt: [NSObject: AnyObject]?
        do {
            propertiesOpt = try self.applicationDocumentsDirectory.resourceValuesForKeys([NSURLIsDirectoryKey])
        } catch var error1 as NSError {
            error = error1
            propertiesOpt = nil
        } catch {
            fatalError()
        }
        if let properties = propertiesOpt {
            if !properties[NSURLIsDirectoryKey]!.boolValue {
                failureReason = "Expected a folder to store application data, found a file \(self.applicationDocumentsDirectory.path)."
                shouldFail = true
            }
        } else if error!.code == NSFileReadNoSuchFileError {
            error = nil
            do {
                try fileManager.createDirectoryAtPath(self.applicationDocumentsDirectory.path!, withIntermediateDirectories: true, attributes: nil)
            } catch var error1 as NSError {
                error = error1
            } catch {
                fatalError()
            }
        }
        
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator?
        if !shouldFail && (error == nil) {
            coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
            let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Sudoku_Image_Solver.storedata")
            do {
                try coordinator!.addPersistentStoreWithType(NSXMLStoreType, configuration: nil, URL: url, options: nil)
            } catch var error1 as NSError {
                error = error1
                coordinator = nil
            } catch {
                fatalError()
            }
        }
        
        if shouldFail || (error != nil) {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            if error != nil {
                dict[NSUnderlyingErrorKey] = error
            }
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSApplication.sharedApplication().presentError(error!)
            return nil
        } else {
            return coordinator
        }
    }()

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving and Undo support

    @IBAction func saveAction(sender: AnyObject!) {
        // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
        if let moc = self.managedObjectContext {
            if !moc.commitEditing() {
                NSLog("\(NSStringFromClass(self.dynamicType)) unable to commit editing before saving")
            }
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    NSApplication.sharedApplication().presentError(error!)
                }
            }
        }
    }

    func windowWillReturnUndoManager(window: NSWindow) -> NSUndoManager? {
        // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
        if let moc = self.managedObjectContext {
            return moc.undoManager
        } else {
            return nil
        }
    }

    func applicationShouldTerminate(sender: NSApplication) -> NSApplicationTerminateReply {
        // Save changes in the application's managed object context before the application terminates.
        
        if let moc = managedObjectContext {
            if !moc.commitEditing() {
                NSLog("\(NSStringFromClass(self.dynamicType)) unable to commit editing to terminate")
                return .TerminateCancel
            }
            
            if !moc.hasChanges {
                return .TerminateNow
            }
            
            var error: NSError? = nil
            do {
                try moc.save()
            } catch let error1 as NSError {
                error = error1
                // Customize this code block to include application-specific recovery steps.
                let result = sender.presentError(error!)
                if (result) {
                    return .TerminateCancel
                }
                
                let question = NSLocalizedString("Could not save changes while quitting. Quit anyway?", comment: "Quit without saves error question message")
                let info = NSLocalizedString("Quitting now will lose any changes you have made since the last successful save", comment: "Quit without saves error question info");
                let quitButton = NSLocalizedString("Quit anyway", comment: "Quit anyway button title")
                let cancelButton = NSLocalizedString("Cancel", comment: "Cancel button title")
                let alert = NSAlert()
                alert.messageText = question
                alert.informativeText = info
                alert.addButtonWithTitle(quitButton)
                alert.addButtonWithTitle(cancelButton)
                
                let answer = alert.runModal()
                if answer == NSAlertFirstButtonReturn {
                    return .TerminateCancel
                }
            }
        }
        // If we got here, it is time to quit.
        return .TerminateNow
    }

}

